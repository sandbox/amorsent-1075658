<?php

/**
 * @file
 * Explanation of Image Tag API hooks.
 */

/**
 * Respond to image_tag actions.
 *
 * This hook allows modules to extend the image tag system
 * by responding when certain actions take place.
 *
 * @param $tag
 *   The image tag object being operated on
 * @param $op
 *   The operation being performed. Possible values:
 *   - "load": Tag has just been loaded from the database.
       This hook can be used to load additional data at this time.
 *   - "insert": Tag is being created (inserted into the database)
 *   - "update": Tag is being updated
 *   - "delete": Tag is being deleted
 *
 * @ingroup hooks
 */
function hook_tag_image_tag_api(&$tag, $op){
  if($tag->type == 'my_type'){
    switch($op){
      case 'load' :
        // Add additional information
        break;
      case 'insert' :
        // Insert additional information into my_module tables
        break;
      case 'update' :
        // Update additional information into my_module tables
        break;
      case 'delete':
        // Delete additional information from my_module tables
        break;
      case 'access' :
      case 'form' :
    }
  }
}



/**
 * Implementation of hook_image_tag_info().
 */
function image_tag_node_image_tag_info() {
  return array(
    'node' => array(
      'name' => t('Node'),
      'module' => 'image_tag_node',
      'description' => t("Tag A Node"),
    ),
  );
}



/**
 * Implementation of image_tag's hook_image_tag_access().
 */
function image_tag_image_tag_access($op, $tag, $account) {
  $type = is_string($tag) ? $tag : (is_array($tag) ? $tag['type'] : $tag->type);

  // Tag belongs to account
  $own = $account->uid == $tag->uid && $account->uid != 0;

  // Let authors view their own tags.
  if ($op == 'view' && $own) return TRUE;

  // Replace 'update' with 'edit'
  $op = $op == 'update' ? 'edit' : $op;

  // Build a permission string and check if the user has this permission
  if($op == 'create') $perm = $op.' '.$type .' tags';
  else $perm = $op.($own ? ' own ' : ' any ').$type .' tags';

  if(user_access($perm)) return IMAGE_TAG_ALLOW;
}