/**
 * Image Tag behavior
 */
Drupal.behaviors.imageTag = function (context) {
  // Attatch Tag behavior
  for (i in Drupal.settings.imageTag) {
    info = Drupal.settings.imageTag[i];
    if($('#' + info.css_id, context).size() == 1){
      taggedImage = new Drupal.imageTagImage(info);
    }
  }
};

/**
 * An annodetative image object
 * @TODO: Layers
 */
Drupal.imageTagImage = function (info) {
  this.mode = 'view';
  this.fid  = info.fid;
  this.presetname  = info.presetname;

  // Select the image
  var img = $('#' + info.css_id);

  // Add the canvas (which has the image as a background) and the containers for the notes
  this.canvas = $('<div id="' + info.css_id + '" class="image-tag-canvas"><div class="image-tag-view"></div><div class="image-tag-edit"><div class="image-tag-edit-box"></div></div></div>')
    .css({
      'background' : 'url("'+ img.attr('src') +'") no-repeat',
      'height' : img.height(),
      'width'  : img.width(),
    });

  this.canvas.children('.image-tag-view, .image-tag-edit').css({
    'height'  : img.height(),
    'width'   : img.width(),
    'display' : 'none',
  });

  // Replace the image with the canvas
  img.replaceWith(this.canvas);

  var image = this;
  // Add the behavior: hide/show the notes when hovering the picture
  this.canvas.hover(
    function() {
      if (image.mode == 'view') {
        $(this).children('.image-tag-view').show(500);
      }
    },
    function() {
      $(this).children('.image-tag-view').hide(500);
    }
  );

  // Add the tags
  this.tags = new Array();

  for (t in info.tags) {
    this.tags[info.tags[t].tid] = new Drupal.imageTagTag(this, info.tags[t]);
  }

  // Add the "Add a Tag" button
  if (info.editable) {
    this.button = $('<a class="image-tag-add" id="image-tag-add-'+ info.field +'">' + Drupal.t('Tag this photo') + '</a>');
    var image = this;
    this.button.click(function(){
      image.addTag();
    });
    this.canvas.prepend(this.button);
  }


  /**
   * Add Tag
   */
  this.addTag = function () {
    if (this.mode == 'view') {
      this.mode = 'edit';
      var image = this;
      // Create/prepare the editable note elements
      var eTag = new Drupal.imageTagTagEditable(this);
      // Load the form and set the draggable/resizable box
      eTag.form.load('/image-tag/add?nid=' + Drupal.settings.imageTag.nid, {}, function() {
  //      Drupal.behaviors.collapse(eTag.form);

        // We add the cancel/close button
        var cancel = $('<a class="image-tag-edit-close">'+ Drupal.t('Cancel') +'</a>');
        cancel.click(function() {
          eTag.destroy();
        });
        eTag.form.prepend(cancel);
        eTag.form.find('#edit-fid').val(image.fid);
        eTag.form.find('#edit-presetname').val(image.presetname);
        eTag.form.pos();

        // Form should submit via ajax
        eTag.form.children('form').ajaxForm({
          type: 'POST',
          success : function(html) {
            eTag.destroy();
            // @TODO: updae without page refresh
            location.reload();
          },
        });
      });
    }
  };
};



/**
 * An image tag
 */
Drupal.imageTagTag = function (image, tag) {

  this.image    = image; // The parent imageTagImage Object

  this.tid      = tag.tid;
  this.fid      = tag.fid;
  this.height   = tag.box_height;
  this.width    = tag.box_width;
  this.left     = tag.box_left;
  this.top      = tag.box_top;
  this.caption  = tag.caption;
  this.actions  = tag.actions;
  this.editable = tag.editable;
  //this.object   = tag.object;

  /**
   * Set the position and size of the box
   */
  this.pos = function() {
    this.box.css({
        'top'   : this.top +'px',
        'left'  : this.left +'px',
        'width' : this.width +'px',
        'height': this.height +'px',
      });

    this.caption.css({
      'left': (this.left) +'px',
      'top' : (parseInt(this.top) + parseInt(this.height) + 2) +'px',
    });
  }

  /**
   * Show the note edit form
   */
  this.edit = function() {
    if (this.image.mode == 'view') {
      this.image.mode = 'edit';
      var tag = this;
      // Create/prepare the editable tag elements
      var eTag = new Drupal.imageTagTagEditable(this.image, this);

      // Load the form and set the draggable/resizable box
      eTag.form.load('/image-tag/' + this.tid + '/edit?nid=' + Drupal.settings.imageTag.nid, {}, function() {
  //      Drupal.behaviors.collapse(editable.note);

        // We add the cancel/close button
        var cancel = $('<a class="image-tag-edit-close">'+ Drupal.t('Cancel') +'</a>');
        cancel.click(function() {
          eTag.destroy();
        });
        eTag.form.prepend(cancel);

        eTag.form.find('#edit-presetname').val(image.presetname);
        eTag.form.pos();

        // Set form to submit via ajax
        eTag.form.children('form').ajaxForm({
          type: 'POST',
          success : function(html) {
            eTag.destroy();
            // @TODO: updae without page refresh
            location.reload();
          },
        });
      });
    }
  };


  // Add the box
  this.box = $('<div class="image-tag-box'+ (this.editable ? ' image-tag-box-editable' : '') +'"><div class = "image-tag-box-inner"></div></div>');

  // Add the caption
  this.caption = $('<div class="image-tag-caption">'+ tag.caption +'</div>').hide();

  // Add the box and caption to the view canvas
  this.image.canvas.children('.image-tag-view')
    .prepend(this.caption)
    .prepend(this.box)
  ;

  // Set the position and size of the tag
  this.pos();

  var tag = this;
  // Add the behavior: hide/display the note when hovering the box
  this.box.hover(
    function() {
      tag.caption.show();
    },
    function() {
      tag.caption.hide();
    }
  );

  this.caption.hover(
    function(){
      tag.caption.show();
      //tag.caption.children('span.actions').show('slow');
    },
    function(){
      tag.caption.hide();
      //tag.caption.children('span.actions').hide();
    }
  );

  // Edit a note feature
  if (this.editable) {
    this.box.click(function () {
      tag.edit();
    });
  }
};



/**
 * The annotation form
 */
Drupal.imageTagTagEditable = function (image, tag) {
  // @TODO: Check for other active forms

  this.image = image;

  /**
   * Destroy the annotation form
   */
  this.destroy = function () {
    this.image.canvas.children('.image-tag-edit').hide();
    this.box
      .resizable('destroy')
      .draggable('destroy')
      .css({
        'top'    : '',
        'left'   : '',
        'width'  : '',
        'height' : '',
      })
    ;
    this.form.remove();
    this.image.mode = 'view';
  };


  // Set up the box
  this.box = this.image.canvas.children('.image-tag-edit').children('.image-tag-edit-box');
  if (tag) {
    this.box.css({
      'top'   : tag.top +'px',
      'left'  : tag.left +'px',
      'width' : tag.width +'px',
      'height': tag.height +'px',
    });
  }

  // Show the edition canvas and hide the view canvas
  this.image.canvas.children('.image-tag-view').hide();
  this.image.canvas.children('.image-tag-edit').show();

  // Add the note (which we'll load with the form afterwards)
  this.form = $('<div id="image-tag-edit-form"></div>')

  var box      = this.box;
  var eTag     = this;
  var form     = this.form;

  // Declare this as a function so we can use it again
  this.form.pos = function(){
    this.css({
      'left': box.offset().left +'px',
      'top' : (parseInt(box.offset().top) + parseInt(box.height()) + 2) +'px',
    });

    // Update form values
    this.find('#edit-box-top').val(box.position().top);
    this.find('#edit-box-left').val(box.position().left);
    this.find('#edit-box-width').val(box.width());
    this.find('#edit-box-height').val(box.height());
  }
  this.form.pos();

  $('body').append(this.form);

  // Set the box as a draggable/resizable element contained in the image canvas.
  // Would be better to use the containment option for resizable but buggy

  this.box.resizable({
    handles: 'all',
    containment: eTag.image.canvas.children('.image-tag-edit'),
    // @TODO: Updating the form values on resize may be overkill, on stop only?
    resize: function(e, ui) { form.pos(); },
    stop: function(e, ui) { form.pos(); },
  });
  this.box.draggable({
    containment: eTag.image.canvas.children('.image-tag-edit'),
    drag: function(e, ui) { form.pos(); },
    stop: function(e, ui) { form.pos(); },
  });
};