<?php

/**
 * @file
 * Views handler : Image tag crop
 */

/**
 * Image Tag Crop Field
 */
class image_tag_crop_handler_field_cropped extends views_handler_field {
  /**
   * Constructor
   */
  function construct() {
    parent::construct();

    // The real field we want is the tag id
    $this->real_field = 'tid';

    // Additional fields
    $this->additional_fields['title'] = array('table' => 'image_tag', 'field' => 'title');
  }

  /**
   * Option Definition
   */
  function option_definition() {
    $options = parent::option_definition();

    // Defaults
    $options['presetname'] = array('default' => '<original crop>');

    return $options;
  }

  /**
   * Options Form
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    // Get a list of available imagecache presets
    $presetnames = array('<original crop>' => '<original crop>');
    $presets = imagecache_presets();
    foreach($presets as $preset) $presetnames[$preset['presetname']] = $preset['presetname'];

    // Imagecache Preset Option
    $form['presetname'] = array(
      '#title' => t('Imagecache Preset'),
      '#type' => 'select',
      '#options' => $presetnames,
      '#required' => TRUE,
      '#default_value' => $this->options['presetname'],
      '#weight' => 4,
    );
  }

  /**
   * Provide text for the administrative summary
   */
  function admin_summary() {
    return t($this->options['presetname']);
  }

  /**
   * Render the field
   */
  function render($values) {
    $tid = $values->tid;
    $presetname = $this->options['presetname'];
    $title = $values->{$this->aliases['title']};
    $alt = $title;
    $attr = NULL;

    // If no preset, value should be null
    $presetname = $presetname != '<original crop>' ? $presetname : NULL;

    // Theme a cropped image
    return theme('image_tag_crop', $tid, $presetname, $alt, $title, $attr);
  }
}