<?php

/**
 * @file
 * Image Tag Crop : Views Integration
 */

/**
 * Implementation of hook_views_data_alter().
 */
function image_tag_crop_views_data_alter(&$cache){
  // Add the Cropped Image Field
  $cache['image_tag']['cropped'] = array(
    'title' => t('Cropped Image'),
    'help' => t('The cropped area of the source image'),
    'field' => array(
      'handler' => 'image_tag_crop_handler_field_cropped',
      'click sortable' => FALSE,
    ),
  );
}

/**
 * Implementation of hook_views_handlers().
 */
function image_tag_crop_views_handlers() {
  // Register views handlers
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'image_tag_crop') . '/views',
    ),
    'handlers' => array(
      'image_tag_crop_handler_field_cropped' => array(
        'parent' => 'views_handler_field',
      ),
    ),
  );
}