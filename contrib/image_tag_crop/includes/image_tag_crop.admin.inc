<?php

/**
 * @file
 * Image Tag Crop : Admin
 */

/*************************************************************************
* Forms                                                                  *
*************************************************************************/

/**
 * Admin Config Form
 */
function image_tag_crop_config_form(){
  $form['crop_settings'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Crop Settings'),
    '#description' => t('Image tags may come in all different dimensions. These settings allow you to standardize the way tags are cropped. If imagecache is producing strange results, these settings may help'),
  );
  $form['crop_settings']['aspect_ratio_standardize'] = array(
    '#type'  => 'checkbox',
    '#title' => t('Standardize crop aspect ratio'),
    '#description' => t('Crop Region will be <em>expanded</em> to fit the aspect ratio defined here.'),
    '#default_value' => variable_get('image_tag_crop_aspect_ratio_standardize', 0),
  );
  $form['crop_settings']['aspect_ratio_value'] = array(
    '#type'  => 'textfield',
    '#title' => t('Crop Aspect Ratio'),
    '#description' => t('Width / Height'),
    '#default_value' => variable_get('image_tag_crop_aspect_ratio', 1),
  );
  $form['crop_settings']['prevent_tiny'] = array(
    '#type'  => 'checkbox',
    '#title' => t('Prevent tiny crops'),
    '#description' => t('If crop region is too small, try to expand the selection'),
    '#default_value' => variable_get('image_tag_crop_prevent_tiny', 0),
  );
  $form['crop_settings']['diagonal_value'] = array(
    '#type'  => 'textfield',
    '#title' => t('Min Diagonal ( attempted )'),
    '#description' => t('If the cropped region is below this value, attempt to expand.'),
    '#default_value' => variable_get('image_tag_crop_diagonal_value', 100),
  );

  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Submit'),
    '#weight' => 50,
  );
  return $form;
}

/**
 * Admin Config Form: Validate
 */
function image_tag_crop_config_form_validate($form_id, &$form_state) {
  if (!is_numeric($form_state['values']['aspect_ratio_value'])) {
    form_set_error('aspect_ratio_value', t('Please enter a number.'));
  }
  if (!is_numeric($form_state['values']['diagonal_value'])) {
    form_set_error('diagonal_value', t('Please enter a number.'));
  }
}

/**
 * Admin Config Form: Submit
 *
 * @TODO: This would be a little easier if using system_settings_form
 */
function image_tag_crop_config_form_submit($form, &$form_state){
  $values = $form_state['values'];

  variable_set('image_tag_crop_aspect_ratio_standardize', (bool)$values['aspect_ratio_standardize']);
  variable_set('image_tag_crop_aspect_ratio', $values['aspect_ratio_value']);

  variable_set('image_tag_crop_prevent_tiny', (bool)$values['prevent_tiny']);
  variable_set('image_tag_crop_diagonal_value', $values['diagonal_value']);

  // Flush the the cropped tags
  image_tag_crop_flush('all');
}
