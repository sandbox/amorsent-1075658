<?php

/**
 * @file
 * Image Tag Crop : Additional Functions
 */

/*************************************************************************
* Menu Callbacks                                                         *
*************************************************************************/

/**
 * Cache and return the tag box image
 * This is largly based on _imagecache_cache
 */
function image_tag_crop_cache($tid_jpg = NULL, $preset = NULL){
  if(!(isset($tid_jpg) && substr($tid_jpg,strlen($tid_jpg) - 4, 4) == '.jpg')){
    // Send a 404 no tag is found.
    header("HTTP/1.0 404 Not Found");
    exit;
  }

  // Drop the file extension and load the tag
  $tid = (int)substr($tid_jpg,0,strlen($tid_jpg) - 4);
  $tag = image_tag_load($tid);

  if (!isset($tag)) {
    // Send a 404 no tag is found.
    header("HTTP/1.0 404 Not Found");
    exit;
  }

  $src = db_result(db_query('SELECT filepath FROM {files} WHERE fid = %d', $tag->fid));
  $dst = file_directory_path() . '/image-tag-crop/' . $tid_jpg;

  // Check if the path to the file exists.
  if (!is_file($src) && !is_file($src = file_create_path($src))) {
    watchdog('image_tag', '404: Unable to find %image ', array('%image' => $src), WATCHDOG_ERROR);
    header("HTTP/1.0 404 Not Found");
    exit;
  };

//  $lockfile = file_directory_temp() . '/image-tag-crop/' . $tid_jpg;
//  if (file_exists($lockfile)) {
//    watchdog('image_tag', 'Image Tag already generating: %dst, Lock file: %tmp.', array('%dst' => $dst, '%tmp' => $lockfile), WATCHDOG_NOTICE);
//    // 307 Temporary Redirect, to myself. Lets hope the image is done next time around.
//    header('Location: '. request_uri(), TRUE, 307);
//    exit;
//  }
//  touch($lockfile);
//  // register the shtdown function to clean up lock files. by the time shutdown
//  // functions are being called the cwd has changed from document root, to
//  // server root so absolute paths must be used for files in shutdown functions.
//  register_shutdown_function('file_delete', realpath($lockfile));

  // Check if derivative exists...
  // (file was created between apaches request handler and reaching this code)
  // otherwise try to create the derivative.
  if (file_exists($dst) || _image_tag_crop_build_derivative($tag)) {
    if($preset && module_exists('imagecache')){
      // If imagecache exists, and a preset is requested, pass it along
      imagecache_cache($preset, 'image-tag-crop', $tag->tid . '.jpg');
    }
    else{
      // Otherwise send the raw crop
      imagecache_transfer($dst);
    }
  }
  // Generate an error if image could not generate.
  watchdog('image_tag', 'Failed generating an image tag crop for tid: %tid.', array('%tid' => $tag->tid), WATCHDOG_ERROR);
  header("HTTP/1.0 500 Internal Server Error");
  exit;
}

/*************************************************************************
* Theme Functions                                                        *
*************************************************************************/

/**
 * Theme function for a cropped image tag
 * @TODO: Support getsize argument like theme_imagecache
 */
function theme_image_tag_crop($tid, $preset = NULL, $alt = '', $title = '', $attr = NULL) {
  // Default attributes
  if (is_null($attr)) {
    $attr['class'] = 'image-tag-crop';
    $attr['class'] .= isset($preset) ? ' imagecache-'. $preset : '';
  }
  $attr = drupal_attributes($attr);

  $url = _image_tag_crop_url($tid, $preset);

  return '<img src="'. $url .'" alt="'. check_plain($alt) .'" title="'. check_plain($title) .'" '. $attr .' />';
}

/**
 * Generate an image for an object
 *
 * @TODO: Just take the object?
 * It'd be good to make the whole object available, but need some logic to identify object
 * this should go into the api, how do purl / spaces do similar tasks?
 */
function theme_image_tag_crop_object($type = 'user', $id = 0, $preset = NULL) {
  // Grab the tid of the biggest available tag of this object
  $query = "SELECT i.tid FROM {image_tag} i INNER JOIN {image_tag_references} r ON i.tid = r.tid WHERE r.type='%s' AND r.id=%d ORDER BY (i.box_height*i.box_width) DESC";
  $tid   = db_result(db_query($query, $type, $id));

  if(!empty($tid)){
    return theme('image_tag_crop', $tid, $preset);
  }
}

/**
 * Theme function for the image_tag_crop_node formatter
 * Wrapper for theme('image_tag_crop_object')
 */
function theme_image_tag_crop_formatter_image_tag_crop_node($element) {
  $preset = 'product';
  $nid = $element['#node']->nid;
  return theme('image_tag_crop_object', 'node', $nid, $preset);
}

/*************************************************************************
* Helper Functions                                                       *
*************************************************************************/

/**
 * Buld Derivative URL
 *
 * @TODO: Support private file downloads
 */
function _image_tag_crop_url($tid, $preset = NULL){
  $url  = $GLOBALS['base_url'] . '/' . file_directory_path();
  $url .= isset($preset) ? '/imagecache/' . $preset : '';
  $url .= '/image-tag-crop/'. $tid .'.jpg';

  // Use the modified date as a query string to bypass browser cache
  // Browser will still be able to cache the image until it is modified again
  $changed = db_result(db_query('SELECT changed FROM {image_tag} WHERE tid = %d', $tid));
  $args = array('absolute' => TRUE, 'query' => $changed);
  return url($url, $args);
}

/**
 * Crop and save the tag box image
 * This is largly based on imagecache_build_derivative
 */
function _image_tag_crop_build_derivative($tag){
  $dir = file_directory_path() . '/image-tag-crop';

  // Build the destination folder tree if it doesn't already exists.
  if (!file_check_directory($dir, FILE_CREATE_DIRECTORY) && !mkdir($dir, 0775, TRUE)) {
    watchdog('image_tag_crop', 'Failed to create image-tag-crop directory: %dir', array('%dir' => $dir), WATCHDOG_ERROR);
    return FALSE;
  }

  // Try to load image
  $src = db_result(db_query('SELECT filepath FROM {files} WHERE fid = %d', $tag->fid));
  if (!(isset($src) && $image = imageapi_image_open($src))) return FALSE;

  $dst = $dir . '/' . $tag->tid . '.jpg';
  if (file_exists($dst)) {
    watchdog('image_tag_crop', 'Cached image file %dst already exists but is being regenerated. There may be an issue with your rewrite configuration.', array('%dst' => $dst), WATCHDOG_WARNING);
  }

  // Set crop region
  $y      = (int)round($tag->box_top);
  $x      = (int)round($tag->box_left);
  $width  = (int)round($tag->box_width);
  $height = (int)round($tag->box_height);

  // Standardize aspect ratio?
  $aspect = variable_get('image_tag_crop_aspect_ratio_standardize', 0) ?
            variable_get('image_tag_crop_aspect_ratio', 1) :
            ( $width / $height );

  // In order to deal with tags of various aspect ratios
  // Expand to a predetermined aspect ratio
  if(variable_get('image_tag_crop_aspect_ratio_standardize', 0)){
    $w_new  = (int)max(($aspect * $height), $width);
    $h_new  = (int)max(($width  / $aspect), $height);

    // Re-center and expand
    $x      = $x - ($w_new - $width)  / 2;
    $y      = $y - ($h_new - $height) / 2;
    $width  = $w_new;
    $height = $h_new;

    // Check bounds
    $x = ($x < 0) ? 0 : $x;
    $y = ($y < 0) ? 0 : $y;
    if(($x + $width)  > $image->info['width'])  $x = $image->info['width']  - $width;
    if(($y + $height) > $image->info['height']) $y = $image->info['height'] - $height;
  }

  // crop area too small?
  if( variable_get('image_tag_crop_prevent_tiny', FALSE )){
    $diagonal = variable_get('image_tag_crop_diagonal_value', 100 );

    if( $diagonal > hypot( $width , $height ) ){
      $h_new = $diagonal / sqrt( pow($aspect, 2) + 1 );
      $w_new = $diagonal / sqrt( ( 1 / pow($aspect, 2) ) + 1 );

      // Re-center and expand
      $x      = $x - ($w_new - $width)  / 2;
      $y      = $y - ($h_new - $height) / 2;
      $width  = $w_new;
      $height = $h_new;

      // Check bounds
      $x = ($x < 0) ? 0 : $x;
      $y = ($y < 0) ? 0 : $y;
      if(($x + $width)  > $image->info['width'])  $x = $image->info['width']  - $width;
      if(($y + $height) > $image->info['height']) $y = $image->info['height'] - $height;
    }
  }


  // Crop image
  imageapi_image_crop(&$image, $x, $y, $width, $height);

  // Try to save cropped image
  if (!imageapi_image_close($image, $dst)) {
    watchdog('image_tag_crop', 'There was an error saving the new image file %dst.', array('%dst' => $dst), WATCHDOG_ERROR);
  }

  return TRUE;
}

/**
 * Flush cached cropped tags
 * If imagecache exists, also flush the derivatives for each preset
 *
 * In most cases, use image_tag_crop_flush($arg) instead
 *
 * @param $arg
 *   The tid of the tag to flush derivatives
 *   or 'all' to flush all derivatives
 *
 * @see image_tag_crop_flush()
 */
function _image_tag_crop_flush($arg) {
  $paths = array();

  // 'all' or tid ?
  if($arg == 'all'){
    $derivative = '/image-tag-crop';
  }
  else if(is_numeric($arg) && $arg == (int)$arg){
    $derivative = '/image-tag-crop/'. $arg .'.jpg';
  }
  else return;

  // Raw derivative
  $path = realpath(file_directory_path() . $derivative);
  if($path) $paths[] = $path;

  // Imagecache derivatives
  if(module_exists('imagecache')){
    // Get a list of presets and build a list of
    // actual paths to image-tag-crop derivatives
    $result = db_query('SELECT presetname FROM {imagecache_preset}');
    while ($preset = db_result($result)){
      $path = realpath(file_directory_path() .'/imagecache/'. $preset . $derivative);
      if($path) $paths[] = $path;
    }
  }

  // Recursively delete these paths
  foreach($paths as $path){
    _imagecache_recursive_delete($path);
  }
}
