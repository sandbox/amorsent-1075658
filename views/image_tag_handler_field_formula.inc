<?php

/**
 * @file
 * Views handler: Formula Field
 */

/**
 * Formula Field
 */
class image_tag_handler_field_formula extends views_handler_field {
  /**
   * Constructor
   */
  function construct() {
    parent::construct();

    if (!empty($this->definition['formula'])) {
      $this->formula = $this->definition['formula'];
    }
  }

  /**
   * Build the query based upon the formula
   */
  function query() {
    $this->ensure_my_table();
    $formula = $this->get_formula();

    // Add the field to the query.
    $this->field_alias = $this->query->add_field(NULL, $formula,  $this->table_alias . '_' . $this->field);

    $this->add_additional_fields();
  }

  function get_formula() {
    return str_replace('***table***', $this->table_alias, $this->formula);
  }
}