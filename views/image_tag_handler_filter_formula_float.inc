<?php

/**
 * @file
 * Views handler: Float Formula Filter
 */

/**
 * Formula Field
 */
class image_tag_handler_filter_formula_float extends views_handler_filter_float {
  /**
   * Constructor
   */
  function construct() {
    parent::construct();

    if (!empty($this->definition['formula'])) {
      $this->formula = $this->definition['formula'];
    }
  }

  /**
   * Build the query based upon the formula
   */
  function query() {
    $this->ensure_my_table();
    $formula = $this->get_formula();

    // Add the filter to the query.
    $info = $this->operators();
    if (!empty($info[$this->operator]['method'])) {
      $this->{$info[$this->operator]['method']}($formula);
    }
  }

  function get_formula() {
    return str_replace('***table***', $this->table_alias, $this->formula);
  }
}
