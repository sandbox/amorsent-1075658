<?php

/**
 * @file
 * Main Views file.
 *
 * This file tells Views what capabilities we
 * provide and where classes are stored.
 */

/*************************************************************************
* Views Hooks                                                            *
*************************************************************************/

/**
 * Implementation of hook_views_data().
 */
function image_tag_views_data() {
  // ----------------------------------------------------------------
  // image_tag table -- basic table information.

  // Define the base group of this table. Fields that don't
  // have a group defined will go into this field by default.
  $data['image_tag']['table']['group'] = t('Image Tag');

  // Advertise this table as a possible base table
  $data['image_tag']['table']['base'] = array(
    'field'  => 'tid',
    'title'  => t('Image Tag'),
    'help'   => t('Image tags attach information to areas in an image'),
    'weight' => 10,
  );

  // For other base tables, explain how we join
  $data['image_tag']['table']['join']= array(
    'files' => array(
      'left_field' => 'fid',
      'field'      => 'fid',
    ),
  );

  // ----------------------------------------------------------------
  // image_tag table -- fields

  // tid
  $data['image_tag']['tid'] = array(
    'title' => t('Tag ID'),
    'help' => t('The tag ID of the tag'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric', // TODO: Custom validator?
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );

  // title
  $data['image_tag']['title'] = array(
    'title' => t('Title'),
    'help' => t('The title of the tag.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
     ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // created
  $data['image_tag']['created'] = array(
    'title' => t('Created'),
    'help' => t('The time that the tag was created'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  // changed
  $data['image_tag']['changed'] = array(
    'title' => t('Changed'),
    'help' => t('The time that the tag was last edited'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  // published status
  $data['image_tag']['status'] = array(
    'title' => t('Published'),
    'help' => t('Whether or not the tag is published.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('Published'),
      'type' => 'yes-no',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // uid field
  $data['image_tag']['uid'] = array(
    'title' => t('Tagged By'),
    'help' => t('The user who created the tag'),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'users',
      'base field' => 'uid',
      'label' => t('user'),
    ),
  );

  // fid field
  $data['image_tag']['fid'] = array(
    'title' => t('File'),
    'help' => t('The file that contains the tag'),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'files',
      'base field' => 'fid',
      'label' => t('file'),
    ),
  );

  // Box Top
  $data['image_tag']['box_top'] = array(
    'title' => t('Box Top'),
    'help' => t('Position tag\'s top edge, in pixels from the original image\'s top edge'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );

  // Box Left
  $data['image_tag']['box_left'] = array(
    'title' => t('Box Left'),
    'help' => t('Position tag\'s left edge, in pixels from the original image\'s left edge'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );

  // Box Width
  $data['image_tag']['box_width'] = array(
    'title' => t('Box Width'),
    'help' => t('Width of the tag in pixels of the orignal image'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );

  // Box Height
  $data['image_tag']['box_height'] = array(
    'title' => t('Box Height'),
    'help' => t('Height of the tag in pixels of the orignal image'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );

  // Tag Area: Calculated
  $data['image_tag']['box_area'] = array(
    'title' => t('Box Area'),
    'help' => t('The area of the tag in sq. pixels of the original image'),
    'field' => array(
      'handler' => 'image_tag_handler_field_formula',
      'formula' => '(***table***.box_height * ***table***.box_width)',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_formula',
      'formula' => array('default' => '(image_tag.box_height*image_tag.box_width)'),
    ),
    'filter' => array(
      'handler' => 'image_tag_handler_filter_formula_float',
      'formula' => '(***table***.box_height * ***table***.box_width)',
    ),
    // Argument not defined, useful?
  );

  // Tag Aspect Ratio: Calculated
  $data['image_tag']['box_aspect'] = array(
    'title' => t('Box Aspect Ratio'),
    'help' => t('The aspect ratio of the tag'),
    'field' => array(
      'handler' => 'image_tag_handler_field_formula',
      'formula' => '(***table***.box_width / ***table***.box_height)',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_formula',
      'formula' => array('default' => '(image_tag.box_width/image_tag.box_height)'),
    ),
    'filter' => array(
      'handler' => 'image_tag_handler_filter_formula_float',
      'formula' => '(***table***.box_width / ***table***.box_height)',
    ),
    // Argument not defined, useful?
  );

  return $data;
}


/**
 * Implementation of hook_views_handlers().
 */
function image_tag_views_handlers() {
  // Register views handlers
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'image_tag') . '/views',
    ),
    'handlers' => array(
      'image_tag_handler_field_formula' => array(
        'parent' => 'views_handler_field',
      ),
      'image_tag_handler_filter_formula_float' => array(
        'parent' => 'views_handler_filter_float',
      ),
    ),
  );
}