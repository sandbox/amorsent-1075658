<?php

/**
 * @file
 * Image Tag API : Theme Functions
 */


/*************************************************************************
* Theme Functions                                                        *
*************************************************************************/

/**
 * Theme Image Tag Interface
 *
 * @TODO: Review Access control
 *
 * @param $file
 *   The file object for the image
 * @param $presetname
 *   (optional) The name of an imagecache preset to use
 * @param $alt
 *   (optional) <img> alt text
 * @param $title
 *   (optional) <img> title text
 * @param $attr
 *   (optional) An associative array of additional attributes,
 *   may be used to override default class and id
 *
 * @ingroup themeable
 */
function theme_image_tag_interface($file, $presetname = NULL, $alt = '', $title = '', $attr = array()){
  drupal_add_js('misc/autocomplete.js'); // @TODO: need a more dynamic js / css solution - quicktabs has a good example

  if (empty($file['fid'])) return ''; // Bail if no fid

  // Default attributes
  $default_attr = array(
    'id' => 'image-tag-fid-' . $file['fid'],
    'class' => 'image-tag' . (isset($presetname) ? ' imagecache-'. $presetname : ''),
  );
  // Merge with provided attributes
  $attr = array_merge($default_attr, $attr);

  // Get the image tags for this file
  $tags = image_tag_get_image_tags($file['fid']);

  // Only send necessary tag fields to javascript.
  $js_tag_def = array(
    'tid',
    //'fid',
    //'uid',
    //'type',
    //'id',
    'title',
    'caption',
    'box_top',
    'box_left',
    'box_width',
    'box_height',
    //'status',
    //'timestamp',
    'editable',
  );
  foreach($tags as $tag){
    foreach(array_keys((array)$tag) as $field) if(!in_array($field, $js_tag_def)) unset($tag->$field);
  }

  // For Imagecache rendering, remap tag coordinates if not possible,
  // $presetname will be reset to NULL and rendering will continue
  if(isset($presetname)){
    module_load_include('inc', 'image_tag', 'includes/image_tag.imagecache_remap');
    $presetname = image_tag_imagecache_remap_tags($tags, 'to', $presetname, $file['filepath']);
  }

  // Build the field settings and pass to javascript
  $settings = array(array(
    'css_id'   => $attr['id'],
    'fid'      => $file['fid'],
    'tags'     => $tags,
    'presetname'   => $presetname,
    'editable' => user_access('administer image tags') || user_access('create image tags'),
  ));

  //Add the current node id if we're on a node page
  $node = menu_get_object();
  if($node) $settings['nid'] = $node->nid;

  drupal_add_js(array('imageTag' => $settings), 'setting');

  // Load all the JS and CSS magic
  $module_path = drupal_get_path('module', 'image_tag');
  drupal_add_css($module_path .'/css/image_tag.css');
  drupal_add_js('misc/jquery.form.js'); // for ajaxForm();
  jquery_ui_add(array('ui.resizable', 'ui.draggable'));
  drupal_add_js($module_path .'/js/image_tag.js');

  if(isset($presetname)){
    return theme('imagecache', $presetname, $file['filepath'], $file['alt'], $file['title'], $attr);
  }
  else {
    return theme('imagefield_image', $file, $file['alt'], $file['title'], $attr);
  }
}

/**
 * Theme Image Tag CCK Field Formatter
 *
 * @param $element
 *   The CCK field element
 *
 * @see image_tag_field_formatter_info()
 *
 * @ingroup themeable
 */
function theme_image_tag_formatter_image_tag($element) {
  // Extract the preset name from the formatter name.
	$presetname = substr($element['#formatter'], strlen('image_tag_'));
  $presetname = !empty($presetname) ? $presetname : NULL;

  $file  = $element['#item'];
  return theme('image_tag_interface', $file, $presetname);
}
