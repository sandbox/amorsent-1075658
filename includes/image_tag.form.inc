<?php

/**
 * @file
 * Image Tag API : Additional Functions
 */

/*************************************************************************
* Menu Callbacks                                                         *
*************************************************************************/

/**
 * Load the image_tag_tag_form via ajax
 */
function image_tag_ajax($tag = NULL){
  print drupal_get_form('image_tag_tag_form', $tag);
}

/*************************************************************************
* Forms                                                                  *
*************************************************************************/

 /**
 * Form builder for the image_tag_tag_form.
 *
 * @TODO: Develop this API a bit
 *
 * @param $tag
 *   The image tag object
 *
 * @see image_tag_tag_form_validate()
 * @see image_tag_tag_form_submit()
 * @ingroup forms
 */
function image_tag_tag_form(&$form_state, $tag){
  // Get a list of tag types for which the user has permission
  $tag_types = array();
  $types = image_tag_get_types();
  foreach($types as $type => $info) if (image_tag_access('create', $type)) $tag_types[$type] = $info['name'];

  $form['#tag'] = $tag;

  // Basic tag information.
  // These elements are just values so they are not even sent to the client.
  foreach (array('tid', 'uid') as $key) {
    $form[$key] = array(
      '#type' => 'value',
      '#value' => isset($tag->$key) ? $tag->$key : NULL,
    );
  }

  // These are hidden values
  $hidden_fields = array(
    'fid',
    'presetname',
    'box_top',
    'box_left',
    'box_width',
    'box_height',
  );

  foreach ($hidden_fields as $key) {
    $form[$key] = array(
      '#title' => t($key),
      '#type' => 'hidden',
      '#default_value' => isset($tag->$key) ? $tag->$key : NULL,
    );
  }

  // references
  $form['references'] = array(
    '#tree'  => TRUE,
    '#type'  => 'fieldset',
    '#title' => t('References'),
  );

  // @TODO: This is sort of an incomplete bit of form processing,
  // but we're going to alter it in product_tag anyway. A full
  // implementation here is currently outside the scope of this custom module

  // Default
  if(empty($tag->references)) {
    $tag->{references}[] = array(
      'type' => '',
      'id'   => NULL,
    );
  }

  foreach($tag->references as $key => $ref){
    // type
    $form['references'][$key]['type'] = array(
      '#type' => 'select',
      '#title' => t('Type'),
      '#options' => $tag_types,
      '#default_value' => isset($ref['type']) ? $ref['type'] : '',
    );

    // id
    $form['references'][$key]['id'] = array(
      '#title' => t('Id'),
      '#type' => 'textfield',
      '#default_value' => isset($ref['id']) ? $ref['id'] : NULL,
    );
  }

  // published
//  $form['status'] = array(
//    '#title' => t('Published'),
//    '#type' => 'checkbox',
//    '#default_value' => isset($tag->status) ? $tag->status : TRUE, // @TODO: Config default
//  );

  // title
  $form['title'] = array(
    '#title' => t('Title'),
    '#type' => 'hidden',
    '#default_value' => isset($tag->title) ? $tag->title : '',
  );

  // Add the buttons.
  $form['buttons'] = array(
    '#prefix' => '<div id="image-tag-buttons">',
    '#suffix' => '</div>',
    '#weight' => 50,
  );
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => empty($tag->tid) ? t('Tag') : t('Save'),
    '#submit' => array('image_tag_tag_form_submit'),
  );

  if (!empty($tag->tid) && user_access('administer image tags')) {
    $form['buttons']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('image_tag_tag_form_delete_submit'),
    );
  }

  return $form;
}

/**
 * Form validation handler for image_tag_tag_form().
 *
 * @TODO: Implement some basic validation, and the api for other modules
 *
 * @see image_tag_tag_form()
 * @see image_tag_tag_form_submit()
 */
//function image_tag_tag_form_validate($form, $form_state){
//
//}

/**
 * Form submit handler for image_tag_tag_form().
 *
 * @see image_tag_tag_form()
 * @see image_tag_tag_form_validate()
 */
function image_tag_tag_form_submit($form, $form_state){
  $tag = (object)$form_state['values'];

  // If there's an imagecache preset, remap tag coordinates
  // if not possible, throw form error
  if(!empty($tag->presetname)){
    $tags = array($tag); // pack tag into array: required by remap function

    // Get the file path for the fid
    $filepath = db_result(db_query('SELECT filepath FROM {files} WHERE fid=%d',$tag->fid));

    module_load_include('inc', 'image_tag', 'includes/image_tag.imagecache_remap');
    $presetname = image_tag_imagecache_remap_tags($tags, 'from', $tag->presetname, $filepath);

    $tag = $tags[0];
  }

  image_tag_save($tag); // unpack tag from array
}


/**
 * Form submit (delete) handler for image_tag_tag_form().
 *
 * @see image_tag_tag_form()
 * @see image_tag_tag_form_validate()
 */
function image_tag_tag_form_delete_submit($form, $form_state){
  $tag = (object)$form_state['values'];
  image_tag_delete($tag->tid);

  // Tag is sucessfully deleted, but without this the form returns a 404
  // preventing the ajax success event from firing, giving the impression
  // that nothing happened. This is a workable fix, but...
  // @TODO: Not sure this is the best approach
  print '';
  exit();
}
