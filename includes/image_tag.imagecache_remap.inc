<?php

/**
 * @file
 * Image Tag API : Imagecache Remap
 * Functions for remaping coordinates for imagecache presets
 *
 * @TODO: Turn This into a more general API for use by other modules
 */

/**
 * Remap tag coordinates to Imagecache preset
 *
 * @TODO: Cache canvas for performance
 * @TODO: Deal with tags out of bounds? or maybe just make css overflow: hidden
 *
 * @param $tags
 *   (by ref) An array of Image Tags
 * @param $path
 *   The path to the image
 * @param $direction
 *   'to' or 'from'
 * @param $presetname
 *   The name of an imagecache preset to use
 *
 * @return
 *  If success, the $presetname. If failure, NULL
 */
function image_tag_imagecache_remap_tags(&$tags, $direction = 'to', $presetname, $path){
  $canvas = image_tag_imagecache_remap_canvas($presetname, $path);

  // Return null if canvas couldn't be calculated
  if(is_null($canvas)) return NULL;

  $new = $canvas['new'];

  // Map tags to imagecache preset dimensions
  if($direction == 'to'){
    foreach($tags as $i => $tag){
      $tag->point_top  = (int)($tag->point_top  - $new['crop_top']  ) * $new['scale_height'];
      $tag->point_left = (int)($tag->point_left - $new['crop_left'] ) * $new['scale_width'];
      $tag->box_top    = (int)($tag->box_top    - $new['crop_top']  ) * $new['scale_height'];
      $tag->box_left   = (int)($tag->box_left   - $new['crop_left'] ) * $new['scale_width'];
      $tag->box_width  = (int)($tag->box_width  * $new['scale_width']);
      $tag->box_height = (int)($tag->box_height * $new['scale_height']);

      // Drop tags that extend outside bounds
      if(  ($tag->box_top  < 0)
        || ($tag->box_left < 0)
        || ($tag->box_top  + $tag->box_height > $canvas['cur']['height'])
        || ($tag->box_left + $tag->box_width  > $canvas['cur']['width'])
      ) unset($tags[$i]);
    }

    return $presetname;
  }

  // Map tags back to original file dimensions
  else if($direction == 'from'){
    foreach($tags as $tag){
      $tag->point_top  = (int)($tag->point_top  / $new['scale_height'] + $new['crop_top']);
      $tag->point_left = (int)($tag->point_left / $new['scale_width']  + $new['crop_left']);
      $tag->box_top    = (int)($tag->box_top    / $new['scale_height'] + $new['crop_top']);
      $tag->box_left   = (int)($tag->box_left   / $new['scale_width']  + $new['crop_left']);
      $tag->box_width  = (int)($tag->box_width  / $new['scale_width']);
      $tag->box_height = (int)($tag->box_height / $new['scale_height']);
    }

    return $presetname;
  }

  // Invalid direction
  else return NULL;
}

/**
 * Get canvas remap info
 *
 * Always scale first, then crop
 */
function image_tag_imagecache_remap_canvas($presetname, $path){
  // Load the preset if it exists
  $preset = imagecache_preset_by_name($presetname);

  if (file_check_location($path, file_directory_path()) && isset($preset)) {
    //$src_img = imageapi_image_open($path);
    list($width, $height, $type, $attr) = getimagesize($path);
    $canvas = array(
      'src' => array(
        'path'   => $path,
        'width'  => $width,
        'height' => $height,
      ),
      'cur' => array(
        'width'  => $width,
        'height' => $height,
      ),
      'new' => array(
        'scale_width'  => 1,
        'scale_height' => 1,
        'crop_top'     => 0,
        'crop_left'    => 0,
        'crop_bottom'  => 0,
        'crop_right'   => 0,
      )
    );

    // Call each action remap action to modify canvas factors
    foreach($preset['actions'] as $action){
      // Prepare function name, don't do anything unless it exists
      $function = 'image_tag_imagecache_remap_' . $action['action'];
      if(function_exists($function)){

        if (!empty($action['data'])) {
          // Make sure the width and height are computed first so they can be used
          // in relative x/yoffsets like 'center' or 'bottom'.
          if (isset($action['data']['width'])) {
            $action['data']['width']   = _imagecache_percent_filter($action['data']['width'], $canvas['cur']['width']);
          }
          if (isset($action['data']['height'])) {
            $action['data']['height']  = _imagecache_percent_filter($action['data']['height'], $canvas['cur']['height']);
          }
          if (isset($action['data']['xoffset'])) {
            $action['data']['xoffset'] = _imagecache_keyword_filter($action['data']['xoffset'], $canvas['cur']['width'], $action['data']['width']);
          }
          if (isset($action['data']['yoffset'])) {
            $action['data']['yoffset'] = _imagecache_keyword_filter($action['data']['yoffset'], $canvas['cur']['height'], $action['data']['height']);
          }
        }

        // Call remap action
        $function($canvas, $action['data']);
      }
      else {
        // If any actions are not implemented, then we can't be sure of
        // maintaining registration with the preset. It might use an
        // action that is not or can not be implemented (ie rotation)
        // Log the error to watchdog, and return NULL to notify of failure
        watchdog('image_tag', 'Unsupported imagecache action %action', array('%action' => $action['action']), WATCHDOG_NOTICE);
        return NULL;
      }
    }

    return $canvas;
  }
  // Not a valid presetname or path
  // Log the error to watchdog, and return NULL to notify of failure
  watchdog('image_tag', 'Not a valid presetname: %presetname or path: %path', array('%presetname' => $presetname, '%path' => $path), WATCHDOG_NOTICE);
  return NULL;
}

/*************************************************************************
* Imagecache Remap Canvas Action Implementations                         *
*************************************************************************/

/**
 * Remap imagecache_resize
 */
function image_tag_imagecache_remap_imagecache_resize(&$canvas, $data) {
  $width  = (int) round($data['width']);
  $height = (int) round($data['height']);

  // Save adjustments relative to original image
  $canvas['new']['scale_width']  *= $width  / $canvas['cur']['width'];
  $canvas['new']['scale_height'] *= $height / $canvas['cur']['height'];

  // Save current dimensions for reference
  $canvas['cur']['width']  = $width;
  $canvas['cur']['height'] = $height;
}

/**
 * Remap imagecache_scale
 */
function image_tag_imagecache_remap_imagecache_scale(&$canvas, $data) {
  $aspect = $canvas['cur']['height'] / $canvas['cur']['width'];
  $width  = $data['width'];
  $height = $data['height'];

  if ($data['upscale']) {
    // Set width/height according to aspect ratio if either is empty.
    $width  = !empty($width)  ? $width  : $height / $aspect;
    $height = !empty($height) ? $height : $width  / $aspect;
  }
  else {
    // Set impossibly large values if the width and height aren't set.
    $width  = !empty($width) ? $width : 9999999;
    $height = !empty($height) ? $height : 9999999;

    // Don't scale up.
    if (round($width) >= $canvas['cur']['width'] && round($height) >= $canvas['cur']['height']) {
      return TRUE;
    }
  }

  if ($aspect < $height / $width) $height = $width * $aspect;
  else $width = $height / $aspect;

  // Pass it on to the resize action
  $data['width'] = $width;
  $data['height'] = $height;
  image_tag_imagecache_remap_imagecache_resize(&$canvas, $data);
}

/**
 * Remap imagecache_deprecated_scale
 */
function image_tag_imagecache_remap_imagecache_deprecated_scale(&$canvas, $data) {
  if ($data['fit'] == 'outside' && $data['width'] && $data['height']) {
    $ratio = $canvas['cur']['width'] / $canvas['cur']['height'];
    $new_ratio = $data['width']/$data['height'];
    $data['width'] = $ratio > $new_ratio ? 0 : $data['width'];
    $data['height'] = $ratio < $new_ratio ? 0 : $data['height'];
  }
  // Set impossibly large values if the width and height aren't set.
  $data['width'] = $data['width'] ? $data['width'] : 9999999;
  $data['height'] = $data['height'] ? $data['height'] : 9999999;

  // Pass it on to the scale action
  image_tag_imagecache_remap_imagecache_scale(&$canvas, $data);
}

/**
 * Remap imagecache_crop
 */
function image_tag_imagecache_remap_imagecache_crop(&$canvas, $data) {
  $aspect = $canvas['cur']['height'] / $canvas['cur']['width'];
  $top = $data['yoffset'];
  $left = $data['xoffset'];
  $width = $data['width'];
  $height = $data['height'];

  if (empty($height)) $height = $width / $aspect;
  if (empty($width)) $width = $height * $aspect;

  $width = (int) round($width);
  $height = (int) round($height);

  // Save adjustments relative to original image
  $canvas['new']['crop_top']    += $top / $canvas['new']['scale_height'];
  $canvas['new']['crop_left']   += $left / $canvas['new']['scale_width'];
  $canvas['new']['crop_right']  += ($canvas['cur']['width']  - ($left + $width) ) / $canvas['new']['scale_width'];
  $canvas['new']['crop_bottom'] += ($canvas['cur']['height'] - ($top  + $height)) / $canvas['new']['scale_height'];

  // Save current dimensions for reference
  $canvas['cur']['width']  = $width;
  $canvas['cur']['height'] = $height;
}

/**
 * Remap imagecache_scale_and_crop
 */
function image_tag_imagecache_remap_imagecache_scale_and_crop(&$canvas, $data) {
  $scale = max($data['width'] / $canvas['cur']['width'], $data['height'] / $canvas['cur']['height']);

  $data['xoffset'] = ($canvas['cur']['width']  * $scale - $data['width'])  / 2;
  $data['yoffset'] = ($canvas['cur']['height'] * $scale - $data['height']) / 2;
  $data['width']   = $canvas['cur']['width']   * $scale;
  $data['height']  = $canvas['cur']['height']  * $scale;

  // Pass on to scale action, then crop action
  image_tag_imagecache_remap_imagecache_resize(&$canvas, $data);
  image_tag_imagecache_remap_imagecache_crop(&$canvas, $data);
}

/*************************************************************************
* Do nothing functions for imagecache actions not effecting dimensions   *
*************************************************************************/

/**
 * Remap imagecache_desaturate
 */
function image_tag_imagecache_remap_imagecache_desaturate(&$canvas, $data = array()) {
  // Function provided only to allow action to work
  // Doesn't effect dimensions, no remaping required
  return TRUE;
}

/**
 * Remap imagecache_sharpen
 */
function image_tag_imagecache_remap_imagecache_sharpen(&$canvas, $data = array()) {
  // Function provided only to allow action to work
  // Doesn't effect dimensions, no remaping required
  return TRUE;
}